from datetime import datetime

from src.helpers.exceptions import UnValidVolumeError

beer_types = [
    {'type': 'half', 'range': [100, 300]},
    {'type': 'pint', 'range': [300, 800]},
    {'type': 'stein', 'range': [800, 1000]}
]


class Beer:
    """
    Beer class which model the beer resource that is stored as an entry in the database. The constructor infers the
    glass type from the volume and has an exception handler that will raise an exception when the volume has no
    corresponding glass type
    """

    def __init__(self, tap_id, volume):
        self.__tap_id = tap_id
        self.__volume = volume
        self.__timestamp = datetime.utcnow()

    """
    Once initialized, the properties should not mutated (we are not considering using an update method) so we expose 
    these properties as such using the @property decorator. State properties are meant to be private here.  
    """

    @property
    def tap_id(self):
        return self.__tap_id

    @property
    def timestamp(self):
        return self.__timestamp

    @property
    def volume(self):
        return self.__volume

    def serialize(self):
        """
        The serialize function takes the data from the beer object and serializes it as JSON
        :return: A serialized version of the beer
        """
        return {
            'volume': self.volume,
            'timestamp': self.timestamp.isoformat(),
            'tapId': self.tap_id
        }

    def describe(self):
        """
        The describe function returns a string describing in a human readable way the contents of the beer object
        :return: A string detailing the beer's properties
        """
        return self.timestamp.isoformat() \
               + ' - tap_id: ' + str(self.tap_id) \
               + ' - ml: ' + str(self.volume)
