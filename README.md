# Altair Client

Python web client for the Altair hands on test

## Table of contents

1. [Building and running the app](#1-building-and-running-the-app)
2. [Technical details](#2-technical-details)
3. [Next steps](#3-next-steps)

## 1. Building and running the app

>  Notes on running the full stack are available at the [altair-server](https://github.com/pedro-rodalia/altair-server) repo.

The client is meant to be run as a Docker container, and takes environment arguments so we can manually set the
`tap_id` and the `inet_addr` so it can communicate with the server. 

```shell script
$ docker run \
        -d \
        --env INET_ADDR=altair-server:8338 \
        --env TAP_ID=1 \
        --network altair-network \
        --name altair-client \
        prodalia/altair-client
```

> The client can also be run as a python process using `$ INET_ADDR=localhost:8338 TAP_ID=1 python client.py`

## 2. Technical details

This application is much simpler than its [altair-server](https://github.com/pedro-rodalia/altair-server) counterpart
because focusing on the server business logic and structured seemed more worthwhile and interesting. 

![Design](/img/design.png)

The client works as a simple runner, that starts its lifecycle upon launch. It will try to connect to the specified 
server and exit if this attempt fails. This is intended so we can delegate on Docker the failover tasks in order to 
keep the client simpler. Another way of handling this issue would be keeping the state of the connection in a flag 
and iterating on the attempts until one of them would succeed. 

> An example of this logic can be seen on my 
>[iot-api](https://github.com/pedro-rodalia/iot-api/blob/3d2f9cdbcca76617c8af5013473efc703fd8e004/app.js#L41) project 
>developed using javascript.

Upon a successful connection, the client would start to pour beer glasses at random intervals and with random volumes
and sending the data over HTTP using the exposed API to the server. It will also notify any incoming message it gets
from the server through the WebSocket connection. 

## 3. Next steps

This is far from a production ready IoT client, but helps the purpose of demoing a system in conjunction with the
server, which has been my focus over the development of this task. However I would like to mention some ideas that 
came to mind while developing the client. 

Currently, the WebSocket channel is wasted, as its just used for logging, but getting feedback about the system state,
even aggregated data can be very useful for an IoT client, helping it to manage its loading effects on the system, for
instance. Or even making its own decisions based on this data without having the server to manage thousands of devices, and
delegating on them, building sort of a distributed system.